package com.zuitt.batch193;

import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int  i = 1, fact = 1;
        int num = 1;

        try{
            System.out.println("Input an integer whose factorial will be computed");
             num = in.nextInt();
        } catch (Exception e){
            System.out.println("Invalid Output");
        } finally{
            while(i <= num ){
                fact *= i;
                i++;
            }
            System.out.println("Factorial of " + num + " is: " +fact);
        }

    }
}
